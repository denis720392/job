package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num1 = 0;
        int num2 = 1;
        while (true){
            System.out.println("Введите N - число последовательности");
            int n = scanner.nextInt();
            int [] numbersFib = new int[n + 1];
            if (n > 0){
                numbersFib[0] = num1;
                numbersFib[1] = num2;
                for (int i = 2; i < n + 1; i++){
                    int temp = numbersFib[i - 2] + numbersFib[i - 1];
                    numbersFib[i] = temp;
                }
                for (int i = 0; i < n + 1; i++){
                    System.out.println((i + 1) + " - " + numbersFib[i]);
                }
                break;
            }else {System.out.println("Введено неверное число N");}
        }
    }
}
